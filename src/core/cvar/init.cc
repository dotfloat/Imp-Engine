#include <core/init.hh>
#include "store.hh"

using namespace imp;
using namespace std::string_view_literals;

IMP_SYSTEM_DECL(gvar, "Console Variables")

IMP_SYSTEM_INIT()
{
    cvar::g_store.reset(new cvar::Store);
}

IMP_SYSTEM_QUIT()
{
    cvar::g_store = nullptr;
}
