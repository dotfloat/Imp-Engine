#include <core/cmd.hh>
#include <core/cvar.hh>
#include <core/cvar/store.hh>

using namespace imp;

namespace {

void s_seta()
{

}

}

namespace imp {
namespace startup {
void init_cvar();
}
}

void imp::startup::init_cvar()
{
    cvar::g_store = new cvar::Store;

    cmd::Register()
        (s_seta, "seta");
}
