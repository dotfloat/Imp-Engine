#ifndef __FLAGS__93667126
#define __FLAGS__93667126

#include <bitset>
#include <initializer_list>

namespace imp::cvar {
  enum class Flag {
      config
  };

  class FlagSet {
      std::bitset<1> m_set {};

  public:
      FlagSet() = default;

      FlagSet(std::initializer_list<Flag> il)
      {
          for (auto f : il) {
              set(f);
          }
      }

      FlagSet& set(Flag flag)
      {
          m_set.set(static_cast<int>(flag));
          return *this;
      }

      FlagSet& set(const FlagSet& flag)
      {
          m_set |= flag.m_set;
          return *this;
      }

      FlagSet& reset(Flag flag)
      {
          m_set.reset(static_cast<int>(flag));
          return *this;
      }

      FlagSet& reset(const FlagSet& flag)
      {
          m_set &= ~flag.m_set;
          return *this;
      }

      bool test(Flag flag) const
      {
          return m_set.test(static_cast<int>(flag));
      }

      bool is_subset_of(const FlagSet& flags) const
      { return (m_set & flags.m_set) == m_set; }
  };
}

#endif //__FLAGS__93667126
