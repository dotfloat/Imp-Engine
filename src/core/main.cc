#include <core/cmd.hh>
#include <core/cvar.hh>
#include <core/init.hh>
#include <core/log.hh>
#include <platform/iplatform.hh>
#include <platform/ivideo.hh>
#include <glbinding/gl/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

using namespace imp;

void imp_init_platform();

namespace {
  imp::ISystem *s_init_list[num_system_types] {};
  std::bitset<num_system_types> s_init_bitset {};

  void quit(size_t index = num_system_types)
  {
      if (index == 0)
          return;

      try {
          for (; index > 0; --index) {
              log::info("Quit " sYELLOWB("{}"), s_init_list[index - 1]->name());
              s_init_list[index - 1]->quit();
          }
      } catch (...) {
          std::cout << "An error occurred while shutting down '"
                    << s_init_list[index - 1]->name()
                    << "', aborting."
                    << std::endl;

          std::abort();
      }

      std::exit(0);
  }

  void init()
  {
      if (!s_init_bitset.all()) {
          throw std::logic_error("Not all systems are available");
      }

      size_t index {};

      try {
          for (; index < num_system_types; ++index) {
              log::info("Init " sYELLOWB("{}"), s_init_list[index]->name());
              s_init_list[index]->init();
          }
      } catch (...) {
          std::cout << "An error occurred while initializing '"
                    << s_init_list[index]->name()
                    << "', quitting."
                    << std::endl;

          quit(index);
      }

      atexit([] { quit(); });
  }
}

ISystem::ISystem(SystemType type)
{
    auto val = static_cast<size_t>(type);
    if (s_init_bitset.test(val)) {
        throw std::logic_error("System already exists");
    }

    s_init_bitset.set(val);
    s_init_list[val] = this;
}

int main(int argc, char **argv)
{
    using namespace gl;
    init();
    imp_init_platform();

    const GLfloat vertices[] = {
        -0.5f, -0.5f, 0.0f,
        1, 0, 0,
        0.5f, -0.5f, 0.0f,
        0, 1, 0,
        0.0f, 0.5f, 0.0f,
        0, 0, 1
    };

    const GLchar *const vglsl = R"(
    #version 330 core
    uniform mat4 u_Projection;
    uniform mat4 u_View;
    uniform mat4 u_Model;
    uniform vec2 u_Screen;

    in vec3 v_Position;
    in vec3 v_Color;

    out vec3 f_Color;

    void main() {
        gl_Position = u_Projection * u_View * u_Model * vec4(v_Position, 1);
        f_Color = v_Color;
    })";

    const GLchar *const fglsl = R"(
    #version 330 core
    in vec3 f_Color;
    out vec3 r_Color;
    void main() {
        r_Color = f_Color;
    })";

    GLuint vsid = glCreateShader(GL_VERTEX_SHADER);
    GLuint fsid = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(vsid, 1, &vglsl, nullptr);
    glCompileShader(vsid);

    glShaderSource(fsid, 1, &fglsl, nullptr);
    glCompileShader(fsid);

    GLuint prog = glCreateProgram();
    glAttachShader(prog, vsid);
    glAttachShader(prog, fsid);
    glLinkProgram(prog);

    glDetachShader(prog, vsid);
    glDetachShader(prog, fsid);

    glDeleteShader(vsid);
    glDeleteShader(fsid);

    glUseProgram(prog);

    auto proj_mat = glm::mat4(1.0);
    auto view_mat = glm::mat4(1.0);
    auto model_mat = glm::mat4(1.0);

    auto proj_unif = glGetUniformLocation(prog, "u_Projection");
    auto view_unif = glGetUniformLocation(prog, "u_View");
    auto model_unif = glGetUniformLocation(prog, "u_Model");
    glUniformMatrix4fv(proj_unif, 1, GL_FALSE, &proj_mat[0][0]);
    glUniformMatrix4fv(view_unif, 1, GL_FALSE, &view_mat[0][0]);
    glUniformMatrix4fv(model_unif, 1, GL_FALSE, &model_mat[0][0]);

    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    GLuint vab;
    glGenBuffers(1, &vab);
    glBindBuffer(GL_ARRAY_BUFFER, vab);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_TRUE, 24, nullptr);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 24, (void*) 12);

    for (;;) {
        g_platform->poll_events();

        g_video->begin_frame();

        view_mat = glm::rotate(view_mat, 0.01f, glm::vec3(0.0f, 1.0f, 0.0f));
        glUniformMatrix4fv(view_unif, 1, GL_FALSE, &view_mat[0][0]);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glDrawArrays(GL_TRIANGLES, 0, 3);

        g_video->end_frame();
    }
}
