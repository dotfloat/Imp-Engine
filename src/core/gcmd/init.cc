#include <core/cmd.hh>
#include <core/init.hh>

using namespace imp;

IMP_SYSTEM_DECL(gcmd, "Console Command")

IMP_SYSTEM_INIT()
{
    cmd::g_store.reset(new cmd::Store);
}

IMP_SYSTEM_QUIT()
{
    cmd::g_store = nullptr;
}
