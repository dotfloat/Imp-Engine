#ifndef __FMT_STRING__77693379
#define __FMT_STRING__77693379

#include "style.hh"

namespace imp::log {
  constexpr auto fmt_init = "Init " sYELLOWB("{}");
  constexpr auto fmt_key_val = sWHITEB("{}") ": {}";
}

#endif //__FMT_STRING__77693379
