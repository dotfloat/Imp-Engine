#include <vector>
#include <iostream>
#include <sstream>
#include <readline/readline.h>
#include <core/cvar.hh>
#include <core/cmd.hh>

using namespace imp;

Vector<cvar::IntVar> vars;

std::ostream& operator<<(std::ostream& s, StringView name)
{ return s.write(name.data(), name.size()); }

void cmd_int(String name, long val, String desc)
{
    std::cout << name << std::endl;
    cvar::IntVar var = val;
    try {
        cvar::g_store->add(var, name, desc);
        vars.emplace_back(std::move(var));
    } catch(std::exception& e) {
        std::cout << "cmd_int: " << e.what() << std::endl;
    }
}

void cmd_whatis(const String& name)
{
    if (auto c = cmd::g_store->find(name); c) {
        std::cout << "Command: " << name << " " << *c << std::endl;
    } else if (auto r = cvar::g_store->find<long>(name); r) {
        std::cout << "Variable: " << name << std::endl;
    } else {
        std::cout << "Not found" << std::endl;
    }
}

void cmd_exit()
{
    std::exit(0);
}

void start_shell()
{
    cmd::g_store->add(cmd_int, "int");
    cmd::g_store->add(cmd_exit, "exit");
    cmd::g_store->add(cmd_exit, "quit");
    cmd::g_store->add(cmd_whatis, "whatis");

    while (char *line = readline("> ")) {
        std::istringstream iss(line);
        free(line);

        Vector<String> args;
        while (!iss.eof()) {
            iss >> args.emplace_back();
        }

        try {
            cvar::IntRef ref(args[0]);
            if (args.size() > 1) {
                ref = boost::lexical_cast<long>(args[1]);
            }
            std::cout << ref.name() << " = " << ref.get() << " (" << ref.desc() << ")\n";
            continue;
        } catch(cvar::ref_error&) {
        }

        auto f = cmd::g_store->find(args[0]);

        if (f) {
            try {
                args.erase(args.begin());
                f->call(args);
            } catch(boost::bad_lexical_cast& e) {
                std::cout << e.what() << std::endl;
            }
        } else {
            std::cout << "Erreur" << std::endl;
        }
    }
}
