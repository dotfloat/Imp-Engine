#ifndef __INIT__97527301
#define __INIT__97527301

#include <prelude.hh>

namespace imp {

  enum struct SystemType {
      gvar,
      gcmd,

      NUM
  };

  constexpr auto num_system_types = static_cast<size_t>(SystemType::NUM);

  class ISystem {
  public:
      ISystem(SystemType type);

      virtual ~ISystem() {}

      virtual StringView name() = 0;
      virtual void init() = 0;
      virtual void quit() = 0;
  };

#define IMP_SYSTEM_DECL(_Type, _Name)           \
  namespace {                                   \
    class s_System : public ::imp::ISystem {    \
    public:                                     \
    s_System(): ISystem(SystemType::_Type) {}   \
    ::imp::StringView name() override           \
        { return _Name; }                       \
    void init() override;                       \
    void quit() override;                       \
    } s_system;                                 \
  }

#define IMP_SYSTEM_INIT()                       \
  void s_System::init()                         \

#define IMP_SYSTEM_QUIT()                       \
  void s_System::quit()                         \

}

#endif //__INIT__97527301
