#pragma once

#include <string>

namespace imp::fs {
  enum Format {
      native_format,
      generic_format,
      auto_format,
      wad_format
  };

  class Path {
  public:
      using value_type = char;
      using string_type = std::string;

      static constexpr value_type preferred_separator = '/';

  private:
      std::string m_str;

  public:
      Path() noexcept = default;
      Path(const Path&) = default;
      Path(Path&&) = default;

      Path(string_type&& source, Format format = auto_format);

      template <class Source>
      Path(const Source& source, Format format = auto_format);
  };
}
