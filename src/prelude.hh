#ifndef __PRELUDE__79955158
#define __PRELUDE__79955158

#include <string>
#include <string_view>
#include <memory>
#include <cstdint>

/**
 * Forward declare a few containers
 */
namespace std {

  template <class T1, class T2>
  struct pair;

  template <class... Types>
  class tuple;

  template <class... Types>
  class variant;

  template <class T>
  class optional;

  template <class T, size_t N>
  struct array;

  template <class T, class Allocator>
  class vector;

  template <class Key, class Compare, class Allocator>
  class set;

  template <class Key, class Hash, class KeyEqual, class Allocator>
  class unordered_set;

  template <class Key, class T, class Compare, class Allocator>
  class map;

  template <class Key, class T, class Hash, class KeyEqual, class Allocator>
  class unordered_map;

}

/**
 * Alias std:: types
 */
namespace imp {

  template <class T1, class T2>
  using Pair = std::pair<T1, T2>;

  template <class... Types>
  using Tuple = std::tuple<Types...>;

  template <class... Types>
  using Variant = std::variant<Types...>;

  template <class T>
  using Optional = std::optional<T>;

  template <class T, size_t N>
  using Array = std::array<T, N>;

  template <class T, class Allocator = std::allocator<T>>
  using Vector = std::vector<T, Allocator>;

  template <
      class Key,
      class Compare = std::less<Key>,
      class Allocator = std::allocator<const Key>
      >
  using TreeSet = std::set<Key, Compare, Allocator>;

  template <
      class Key,
      class Hash = std::hash<Key>,
      class KeyEqual = std::equal_to<Key>,
      class Allocator = std::allocator<const Key>
      >
  using HashSet = std::unordered_set<Key, Hash, KeyEqual, Allocator>;

  template <
      class Key,
      class T,
      class Compare = std::less<Key>,
      class Allocator = std::allocator<std::pair<const Key, T>>
      >
  using TreeMap = std::map<Key, T, Compare, Allocator>;

  template <
      class Key,
      class T,
      class Hash = std::hash<Key>,
      class KeyEqual = std::equal_to<Key>,
      class Allocator = std::allocator<std::pair<const Key, T>>
      >
  using HashMap = std::unordered_map<Key, T, Hash, KeyEqual, Allocator>;

  using Nullptr = std::nullptr_t;

  using String = std::string;

  using StringView = std::string_view;

  template <class T, class Deleter = std::default_delete<T>>
  using Box = std::unique_ptr<T, Deleter>;

  template <class T>
  using Arc = std::shared_ptr<T>;

  template <class T>
  using Weak = std::weak_ptr<T>;

}

/**
 * Define ArrayView
 *
 * Immutable view into a contiguous memory container
 */
namespace imp {

  template <class T>
  class ArrayView {
  public:
      using value_type = const T;
      using reference = const T&;
      using pointer = const T*;
      using iterator = const T*;
      using reverse_iterator = std::reverse_iterator<iterator>;

      using const_reference = reference;
      using const_pointer = pointer;
      using const_iterator = iterator;
      using const_reverse_iterator = reverse_iterator;

  private:
      const T *m_data {};
      size_t m_size {};

  public:
      constexpr ArrayView() noexcept {}

      constexpr ArrayView(const T *data, size_t size) noexcept:
      m_data(data),
          m_size(size) {}

      template <class Allocator>
      ArrayView(const Vector<T, Allocator>& other):
          m_data(other.data()),
          m_size(other.size()) {}

      template <size_t N>
      constexpr ArrayView(const Array<T, N>& other):
          m_data(other.data()),
          m_size(other.size()) {}

      template <size_t N>
      constexpr ArrayView(const T (&other)[N]):
      m_data(other),
          m_size(N) {}

      template <class Allocator>
      ArrayView(const std::basic_string<T, Allocator>& other):
          m_data(other.data()),
          m_size(other.size()) {}

      constexpr ArrayView(const std::basic_string_view<T>& other):
      m_data(other.data()),
          m_size(other.size()) {}

      constexpr ArrayView& operator=(const ArrayView&) = default;

      constexpr ArrayView& operator=(Nullptr)
      {
          m_data = nullptr;
          m_size = 0;
          return *this;
      }

      size_t size() const
      { return m_size; }

      template <class Allocator = std::allocator<T>>
      std::basic_string<T> to_string(Allocator allocator = {})
      { return { begin(), end(), allocator }; }

      constexpr const_reference operator[](size_t idx) const
      { return m_data[idx]; }

      constexpr const_iterator begin() const
      { return m_data; }

      constexpr const_iterator cbegin() const
      { return m_data; }

      constexpr const_iterator rbegin() const
      { return end(); }

      constexpr const_iterator crbegin() const
      { return end(); }

      constexpr const_iterator end() const
      { return m_data + m_size; }

      constexpr const_iterator cend() const
      { return m_data + m_size; }

      constexpr const_iterator rend() const
      { return begin(); }

      constexpr const_iterator crend() const
      { return begin(); }
  };
}

/**
 * Int types
 */

namespace imp {

  using int8 = std::int8_t;
  using int16 = std::int16_t;
  using int32 = std::int32_t;
  using int64 = std::int64_t;

  using uint8 = std::uint8_t;
  using uint16 = std::uint16_t;
  using uint32 = std::uint32_t;
  using uint64 = std::uint64_t;

  constexpr int8 operator""_i8(unsigned long long x)
  { return static_cast<int8>(x); }

  constexpr int16 operator""_i16(unsigned long long x)
  { return static_cast<int16>(x); }

  constexpr int32 operator""_i32(unsigned long long x)
  { return static_cast<int32>(x); }

  constexpr int64 operator""_i64(unsigned long long x)
  { return static_cast<int64>(x); }

  constexpr uint8 operator""_u8(unsigned long long x)
  { return static_cast<uint8>(x); }

  constexpr uint16 operator""_u16(unsigned long long x)
  { return static_cast<uint16>(x); }

  constexpr uint32 operator""_u32(unsigned long long x)
  { return static_cast<uint32>(x); }

  constexpr uint64 operator""_u64(unsigned long long x)
  { return static_cast<uint64>(x); }

}

#ifdef IMP_DEBUG
# undef IMP_DEBUG
# define IMP_DEBUG 1

# define IMP_DEBUGEXPR(expr) expr
#else
# define IMP_DEBUG 0

# define IMP_DEBUGEXPR(expr)
#endif

#endif //__PRELUDE__79955158
