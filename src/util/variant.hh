#ifndef __VARIANT__14347857
#define __VARIANT__14347857

#include <prelude.hh>

#ifdef HAVE_STD_VARIANT
#include <variant>
#else
#include <boost/variant.hpp>
#endif

namespace ix {

template <class Visitor, class... Variants>
constexpr auto visit(Visitor&& vis, Variants&&... vars)
{
#ifdef HAVE_STD_VARIANT
    return std::visit(std::forward<Visitor>(vis), std::forward<Variants>(vars)...);
#else
    return boost::apply_visitor(std::forward<Visitor>(vis), std::forward<Variants>(vars)...);
#endif
}

}

#endif //__VARIANT__14347857
