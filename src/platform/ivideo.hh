#ifndef __IVIDEO__97717848
#define __IVIDEO__97717848

#include <prelude.hh>

namespace imp {
  enum struct WindowMode {
      window, // Windowed mode
      exclusive, // Exclusive fullscreen
      noborder // Noborder (fake) fullscreen
  };

  struct VideoMode {
      int width;
      int height;
      WindowMode window_mode;
  };

  class IVideo {
  public:
      virtual ~IVideo() {}

      virtual bool set_mode() = 0;

      virtual bool set_mode(VideoMode) = 0;

      virtual void begin_frame() = 0;

      virtual void end_frame() = 0;
  };

  extern Box<IVideo> g_video;
}

#endif //__IVIDEO__97717848
