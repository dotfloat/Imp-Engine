#include "SDL.h"
#include "private.hh"
#include <core/log.hh>
#include <filesystem>
#include <optional>

using namespace imp;
namespace priv = imp::sdl2;

namespace {
  Optional<std::filesystem::path> s_base_data_dir() noexcept
  {
      std::filesystem::path path;

      auto xdg_dir = getenv("XDG_DATA_DIR");
      if (xdg_dir && xdg_dir[0]) {
          path.assign(xdg_dir);
          path /= "Imp";
          return path;
      }

      auto home = getenv("HOME");
      if (home && home[0]) {
          path.assign(home);
          path /= ".Imp";
          return path;
      }

      return std::nullopt;
  }
}

//
// Platform::Platform
//
priv::Platform::Platform()
{
    log::info(log::fmt_init, "SDL 2");

    /* Print version information */
    SDL_version link, compiled;
    SDL_VERSION(&compiled);
    SDL_GetVersion(&link);
    log::info("Compiled against SDL {}.{}.{}", compiled.major, compiled.minor, compiled.patch);
    log::info("Using SDL {}.{}.{} library", link.major, link.minor, link.patch);

    /* Init */
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) < 0) {
        throw std::runtime_error { "Failed to initialise SDL2" };
    }
}

//
// Platform::~Platform
//
priv::Platform::~Platform()
{
    SDL_Quit();
}

//
// Platform::poll_events
//
void priv::Platform::poll_events()
{
    SDL_Event ev;
    while (SDL_PollEvent(&ev)) {
        switch (ev.type) {
        case SDL_QUIT:
            exit(0);
            break;

        default:
            break;
        }
    }
}

//
// Platform::user_data_dir
//
std::filesystem::path priv::Platform::user_data_dir()
{
    auto base_dir = s_base_data_dir();
    if (!base_dir) {
        throw std::runtime_error { "Couldn't find user data directory" };
    }

    return *base_dir;
}
