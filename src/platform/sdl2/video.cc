#include "SDL.h"
#include "private.hh"
#include <core/log.hh>
#include <glbinding/Binding.h>
#include <glbinding/gl/gl.h>
#include <glm/glm.hpp>

using namespace imp;
namespace priv = imp::sdl2;

namespace {
  void s_sanitize_mode(VideoMode& vm)
  {
      vm.width = std::max(vm.width, 640);
      vm.height = std::max(vm.height, 480);
  }
}

//
// Video::Video
//
priv::Video::Video()
{
    cvar::Register()
        (c_width, "v_Width", "Window width")
        (c_height, "v_Height", "Window height")
        (c_window_mode, "v_WindowMode", "Windowed")
        (c_renderer, "v_Renderer", "Renderer type");

    this->set_mode();
}

//
// Video::~Video
//
priv::Video::~Video()
{
    if (m_glcontext) {
        SDL_GL_DeleteContext(m_glcontext);
    }

    if (m_window) {
        SDL_DestroyWindow(m_window);
    }
}

//
// Video::set_mode
//
bool priv::Video::set_mode()
{
    using Wm = WindowMode;
    Wm wm;
    switch (c_window_mode.get()) {
    case 0:
        wm = Wm::window;
        break;

    case 1:
        wm = Wm::exclusive;
        break;

    case 2:
        wm = Wm::noborder;
        break;

    default:
        wm = Wm::window;
        // log::warn("Invalid {} value: {}", c_window_mode.name(), c_window_mode.get());
        break;
    }

    VideoMode mode {
        c_width,
        c_height,
        wm
    };

    return this->set_mode(mode);
}

//
// Video::set_mode
//
bool priv::Video::set_mode(VideoMode mode)
{
    s_sanitize_mode(mode);

    if (m_initialized) {
        return m_change_mode(mode);
    } else {
        return m_init_mode(mode);
    }
}

//
// Video::m_init_mode
//
bool priv::Video::m_init_mode(const VideoMode& mode)
{
    using namespace glbinding;
    using namespace gl;
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

    Binding::initialize(SDL_GL_GetProcAddress);
    setCallbackMaskExcept(CallbackMask::After, { "glGetError" });
    setAfterCallback([](const FunctionCall& fn){
        const auto error = glGetError();
        if (error != GL_NO_ERROR) {
            std::cout << "GLError in " << fn.toString() << ": " << std::hex << error << std::endl;
        }
    });

    m_window = SDL_CreateWindow("SDL2 Window", 0, 0, mode.width, mode.height, SDL_WINDOW_OPENGL);
    if (!m_window) {
        return false;
    }

    m_glcontext = SDL_GL_CreateContext(m_window);
    if (!m_glcontext) {
        SDL_DestroyWindow(m_window);
        return false;
    }

    log::info("Init " sYELLOWB("{}"), "OpenGL 3.3 Core");

    log::info(sWHITEB("GL_VENDOR") ": {}", glGetString(GL_VENDOR));
    log::info(sWHITEB("GL_RENDERER") ": {}", glGetString(GL_RENDERER));
    log::info(sWHITEB("GL_VERSION") ": {}", glGetString(GL_VERSION));
    log::info(sWHITEB("GLSL_VERSION") ": {}", glGetString(GL_SHADING_LANGUAGE_VERSION));
}

//
// Video::m_change_mode
//
bool priv::Video::m_change_mode(const VideoMode& mode)
{
    return false;
}

//
// Video::begin_frame
//
void priv::Video::begin_frame()
{
}

//
// Video::end_frame
//
void priv::Video::end_frame()
{
    SDL_GL_SwapWindow(m_window);
}
