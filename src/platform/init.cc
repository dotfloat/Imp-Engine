#include <prelude.hh>
#include "iplatform.hh"
#include "ivideo.hh"

// Platform init functions
void imp_init_platform_sdl2();

using namespace imp;

Box<IVideo> imp::g_video {};
Box<IPlatform> imp::g_platform {};

void imp_init_platform()
{
    imp_init_platform_sdl2();
}
