#ifndef __PROGRAM_OPTIONS__13123835
#define __PROGRAM_OPTIONS__13123835

#include <unordered_map>
#include <string_view>
#include <boost/lexical_cast.hpp>
#include <memory>

template <class Key, class Value>
using HashMap = std::unordered_map<Key, Value>;

template <class T, class Delete = std::default_delete<T>>
using UPtr = std::unique_ptr<T, Delete>;

template <class T>
using SPtr = std::shared_ptr<T>;

namespace imp::program_options {

  class OptionsInit;

  class Option {
  };

  struct OptionValue {
      virtual ~OptionValue() {}
      virtual void set(std::string_view) = 0;
  };

  using UOptionValue = UPtr<OptionValue>;

  class Description {
      HashMap<std::string_view, UOptionValue> m_option_map;
  public:
  };

  template <class T>
  UOptionValue value(T& val)
  {
      struct Option : OptionValue {
          T& val;

          void set(std::string_view arg) override
          { val = boost::lexical_cast<T>(arg); }
      };

      return new Option { val };
  }

  UOptionValue cvar(std::string_view name);

  UOptionValue cvar_flag(std::string_view name, std::string_view value);
}


#endif //__PROGRAM_OPTIONS__13123835
