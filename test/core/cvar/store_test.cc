#include <algorithm>
#include <gtest/gtest.h>
#include <core/cvar/var.hh>
#include <core/cvar/ref.hh>
#include <core/cvar/store.hh>

using namespace imp;

namespace {
  void s_add_vars(cvar::Store& ctx) {
      static cvar::IntVar var1;
      static cvar::IntVar var2;
      static cvar::IntVar var3;

      ctx.add(var1, "dong", "dong");
      ctx.add(var2, "Donger", "donger");
      ctx.add(var3, "danger", "danger");
  }
}

TEST(CvarStore, find_nonexistent) {
    cvar::Store ctx;

    auto opt = ctx.find<int64>("anything");

    ASSERT_FALSE(opt.has_value());
}

TEST(CvarStore, add_and_find_var) {
    cvar::Store ctx;
    cvar::IntVar var = 42;

    ctx.add(var, "myVar", "Test var");

    auto optref = ctx.find<int64>("myVar");

    ASSERT_TRUE(optref.has_value());
    ASSERT_EQ(optref->get(), 42);
}

TEST(CvarStore, add_and_find_var2) {
    cvar::Store ctx;
    cvar::IntVar var = 42;

    ctx.add(var, "myVar", "Test var");

    // This time we grab the ref inside the opt. Might fail if the constructors
    // are wrong.
    auto optref = ctx.find<int64>("myVar");
    auto ref = optref.value();

    ASSERT_EQ(ref.get(), 42);
}

TEST(CvarStore, add_and_find_var_icase) {
    cvar::Store ctx;
    cvar::IntVar var = 42;

    ctx.add(var, "myVar", "Test var");

    auto optref = ctx.find<int64>("MYvAr");

    ASSERT_TRUE(optref.has_value());
    ASSERT_EQ(optref->get(), 42);
}

TEST(CvarStore, add_out_of_scope) {
    cvar::Store ctx;

    {
        cvar::IntVar var = 42;
        ctx.add(var, "myVar", "Test var");
    }

    auto optref = ctx.find<int64>("myVar");

    ASSERT_FALSE(optref.has_value());
}

TEST(CvarStore, add_same_name_var) {
    cvar::Store ctx;
    cvar::IntVar var;

    ctx.add(var, "myVar", "Test var");
    const auto methrow = [&] { ctx.add(var, "myVar", "Should throw"); };

    ASSERT_THROW(methrow(), cvar::var_already_exists);
}

TEST(CvarStore, iterate_all) {
    cvar::Store ctx;
    s_add_vars(ctx);

    auto length = std::distance(ctx.begin(), ctx.end());
    ASSERT_EQ(length, 3);
}

TEST(CvarStore, flags_none) {
    cvar::IntVar var = 1;
    cvar::Store ctx;
    ctx.add(var, "var", "Test Var");

    ASSERT_FALSE(var.is_config());

    auto ref = *ctx.find<int64>("var");
    ASSERT_FALSE(ref.is_config());
}

TEST(CvarStore, flags_config) {
    cvar::IntVar var = 1;
    cvar::Store ctx;
    ctx.add(var, "var", "Test Var", { cvar::Flag::config });

    ASSERT_TRUE(var.is_config());

    auto ref = *ctx.find<int64>("var");
    ASSERT_TRUE(ref.is_config());
}

TEST(CvarStore, iterate_config) {
    cvar::IntVar var1  = 0;
    cvar::IntVar cvar1 = 1;
    cvar::IntVar var2  = 2;
    cvar::IntVar cvar2 = 3;
    cvar::Store ctx;
    ctx.add(var1, "dangeroux", "Normal var 1");
    ctx.add(cvar1, "dong", "Config Var 1", { cvar::Flag::config });
    ctx.add(var2,  "donger",  "Normal Var 2");
    ctx.add(cvar2, "Danger", "Config Var 2", { cvar::Flag::config });

    auto range = ctx.iter_flags({ cvar::Flag::config });
    auto it = range.begin();
    ASSERT_EQ((it++)->name(), "Danger");
    ASSERT_EQ((it++)->name(), "dong");
    ASSERT_EQ(it, range.end());
}

TEST(CvarStore, iterate_prefix) {
    cvar::Store ctx;
    s_add_vars(ctx);

    auto range = ctx.iter_prefix("do");
    auto it = range.begin();
    ASSERT_EQ((it++)->name(), "dong");
    ASSERT_EQ((it++)->name(), "Donger");
    ASSERT_EQ(it, range.end());
}

TEST(CvarStore, iterate_all_ref) {
    cvar::Store ctx;
    s_add_vars(ctx);

    auto it = ctx.begin();
    ASSERT_EQ(it->name(), "danger");
    ASSERT_EQ((++it)->name(), "dong");
    ASSERT_EQ((it++)->name(), "dong");
    ASSERT_EQ((*it).name(), "Donger");
    ASSERT_EQ(++it, ctx.end());
}
