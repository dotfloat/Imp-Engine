#include <gtest/gtest.h>
#include <core/cvar/var.hh>

using namespace imp;
using namespace std::string_literals;

TEST(CvarVar, default_init)
{
    cvar::IntVar test;

    ASSERT_EQ(test.get(), 0);
}

TEST(CvarVar, init)
{
    cvar::IntVar test = 1;

    ASSERT_EQ(test.get(), 1);
}

TEST(CvarVar, assign)
{
    cvar::IntVar test = 2;
    test = 3;

    ASSERT_EQ(test.get(), 3);
}

TEST(CvarVar, set_to_default)
{
    cvar::IntVar test = 1;
    test = 2;
    test.set_to_default();

    ASSERT_EQ(test.get(), 1);
}

TEST(CvarVar, set_to_default_with_callback)
{
    bool callback_called {};

    cvar::IntVar test = 1;
    test = 2;
    test.set_callback([&](const int64&) {
        callback_called = true;
    });
    test.set_to_default();

    ASSERT_EQ(test.get(), 1);
    ASSERT_TRUE(callback_called);
}

TEST(CvarVar, set_to_default_with_inhibit_callback)
{
    bool callback_called {};

    cvar::IntVar test = 1;
    test = 2;
    test.set_callback([&](const int64&) {
        callback_called = true;
    });
    test.set_to_default(true);

    ASSERT_EQ(test.get(), 1);
    ASSERT_FALSE(callback_called);
}

TEST(CvarVar, op_add_scalar)
{
    cvar::IntVar test = 41;
    test += 1;

    ASSERT_EQ(test.get(), 42);

    cvar::IntVar other = 10;
    test += other;

    ASSERT_EQ(test.get(), 52);
    ASSERT_EQ(other.get(), 10);
}

TEST(CvarVar, op_subtract_scalar)
{
    cvar::IntVar test = 41;
    test -= 1;

    ASSERT_EQ(test.get(), 40);

    cvar::IntVar other = 10;
    test -= other;

    ASSERT_EQ(test.get(), 30);
    ASSERT_EQ(other.get(), 10);
}

TEST(CvarVar, str_init)
{
    cvar::StrVar test = "hello"s;
    ASSERT_EQ(test.get(), "hello");
}

TEST(CvarVar, str_assign)
{
    cvar::StrVar test { "hello" };
    test = "world";
    ASSERT_EQ(test.get(), "world");
}

TEST(CvarVar, callback)
{
    bool callback_called {};

    cvar::IntVar test = 1;
    test.set_callback([&](const int& new_value) {
        ASSERT_EQ(new_value, 2);
        callback_called = true;
    });

    test = 2;
    ASSERT_TRUE(callback_called);
}

TEST(CvarVar, callback_add)
{
    bool callback_called {};

    cvar::IntVar test = 1;
    test.set_callback([&](const int& new_value) {
        ASSERT_EQ(new_value, 10);
        callback_called = true;
    });

    test += 9;
    ASSERT_TRUE(callback_called);
}

TEST(CvarVar, str_callback)
{
    bool callback_called {};

    cvar::StrVar test = { "hello" };
    test.set_callback([&](const String& new_value) {
        ASSERT_EQ(new_value, "world");
        callback_called = true;
    });

    test = "world";
    ASSERT_TRUE(callback_called);
}
