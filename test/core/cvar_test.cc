#include <gtest/gtest.h>
#include <core/cvar/context.hh>
#include <core/cvar/var.hh>
#include <core/cvar/ref.hh>

using namespace imp;

TEST(Cvar, ref_assign)
{
    cvar::IntVar var = 1;
    cvar::IntRef ref = var;

    ASSERT_EQ(ref.get(), var.get());
}

TEST(Cvar, context_create)
{
    cvar::Context ctx;
}
